let clickCount = 0; /*no ofclicks to match */
let matchCount = 0; /* show the no of matches */
let imgClicked1; /* to get the element of the ids */
let imgClicked2;
let id1;
let id2;
let frstClickSrc; /* to compare the elements*/
let secondClickSrc; /* to compare the elements*/
let count = 1; /* to give it to each img */
let timer = 60; /* timer */

let getScore = Number(localStorage.getItem("bestScore")); /* local stored score */

/* store id */

let idArray = []


/* Game container */
const gameContainer = document.getElementById("game");
gameContainer.style.display = 'none'



// start game page 
const headerEl = document.createElement('header')
headerEl.classList.add('headerContainer')

const headerCardEl = document.createElement('div')
headerCardEl.classList.add('headerCard')

const header1 = document.createElement('h1')
header1.textContent = 'MATCH'

const header2 = document.createElement('h1')
header2.textContent = '-A-'

const header3 = document.createElement('h1')
header3.textContent = 'GIF!'

const scorePara = document.createElement('p')
scorePara.textContent = `BEST SCORE:- ${getScore} `
scorePara.classList.add('scorePara')

const startBtn = document.createElement('button')
startBtn.textContent = 'START!'
startBtn.classList.add('startBtn')
startBtn.addEventListener('click', startGame)

headerCardEl.append(header1, header2, header3, scorePara, startBtn)

headerEl.appendChild(headerCardEl)

document.body.prepend(headerEl)

// create counter
let countEl = document.createElement('div')
countEl.style.display = 'flex'
countEl.style.background = 'white'
countEl.id = 'clickCountCont'

let paraCount = document.createElement('p')
paraCount.id = 'countPara'
paraCount.style.width = '80%'
paraCount.style.backgroundColor = '#5319e6'
paraCount.style.padding = '.5rem'
paraCount.style.color = 'white'
paraCount.textContent = timer


// restart game page 

const footerContainer = document.createElement('footer')
footerContainer.classList.add('footerContainer')

const endCard = document.createElement('div')
endCard.classList.add('endCard')

const footerHeaderEl = document.createElement('h1')
footerHeaderEl.textContent = 'GAME OVER!'

const bestScoreEl = document.createElement('h1')


const restartBtnEl = document.createElement('button')
restartBtnEl.textContent = 'PLAY AGAIN!'
restartBtnEl.classList.add('restartBtn')
restartBtnEl.addEventListener('click', restartGame)

endCard.append(footerHeaderEl, bestScoreEl, restartBtnEl)

footerContainer.appendChild(endCard)

document.body.append(footerContainer)



let IdCounter = 0;

function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}


function createDivsForGifs(array) {

  for (let gif of array) {

    IdCounter++

    if(IdCounter <= ((array.length)/2 )|| IdCounter > ((array.length)/2)){
      const newDiv = document.createElement("div");
      newDiv.id = IdCounter
      newDiv.classList.add('imgDiv');
      newDiv.addEventListener("click", handleCardClick);

      const imgEl = document.createElement('img')
      imgEl.src = 'gifs/'+gif
      imgEl.classList.add('imgSize', 'hideImg')
      newDiv.appendChild(imgEl)

      gameContainer.append(newDiv);

    }
    if(IdCounter == ((array.length)/2) ){
      countEl.appendChild(paraCount)
      gameContainer.appendChild(countEl)
      IdCounter++
    }

  }
}


function handleCardClick(event) {
  clickCount += 1;

  if(clickCount === 1){
    console.log('click1');
    console.log(event.target);
    id1 = event.target.id
    event.target.removeEventListener('click', handleCardClick)
    imgClicked1 = document.getElementById(id1).querySelector('img')
    imgClicked1.classList.remove('hideImg')
    frstClickSrc = imgClicked1.src
  }
  
  else if(clickCount === 2){
    console.log('click2');
    console.log(event.target);
    event.target.removeEventListener('click', handleCardClick)
    id2 = event.target.id
    imgClicked2 = document.getElementById(id2).querySelector('img')
    imgClicked2.classList.remove('hideImg')
    secondClickSrc = imgClicked2.src

    if(frstClickSrc === secondClickSrc){
      matchCount++
      clickCount = 0;
    }
    else{
      resetImagesAndAddEventListener(imgClicked1,imgClicked2) 
      document.getElementById(id1).addEventListener('click', handleCardClick)
      document.getElementById(id2).addEventListener('click', handleCardClick)
    }
  }

  
 
}


const gifsArray = []

for (let i=1; i<13; i++){
  gifsArray.push(i+".gif")
}

const shuffelGifs = shuffle(gifsArray)

const resultGifs = [...shuffelGifs, ...shuffle(shuffelGifs)]


function startGame(){
  headerEl.style.display = 'none'
  gameContainer.style.display = 'flex'
  gameContainer.classList.add('gameContainer')

  createDivsForGifs(resultGifs);/* adding shuffel gifs to game container*/

  let timerId = setInterval(() => {
    timer -= 1
    paraCount.textContent = timer

    if (paraCount.textContent <= 15) {
      paraCount.style.backgroundColor = 'red'
    }

    if (paraCount.textContent == 0) {
      clearInterval(timerId);
      gameContainer.style.display = 'none'
      footerContainer.style.display = 'flex'
      bestScoreEl.textContent = `Best Score : ${matchCount}`
      if (matchCount > getScore) {
        localStorage.setItem("bestScore", matchCount);
      }
    }
  }, 1000)

  
}

function resetImagesAndAddEventListener(arg1,arg2){

  setTimeout(()=>{
    arg1.classList.add('hideImg')
    arg2.classList.add('hideImg')
    clickCount = 0;
  },1000)

}


// restart game 

function restartGame() {
  window.location.href = window.location.href;
}